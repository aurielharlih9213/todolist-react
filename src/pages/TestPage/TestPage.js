import React from "react";


export const TestPage = ({ handleComplete }) => {
  return (
    <div className="test">
      <div className="header">
        <h1>TEST PAGE</h1>
        <button onClick={() => handleComplete()}>Complete</button>
      </div>
      {/* PUT TESTING COMPONENT IN THIS DIV*/}

    </div>
  );
};
